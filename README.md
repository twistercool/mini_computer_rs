# mini\_computer\_rs

This project is a rewrite of mini\_computer, a C library, in Rust.
It also implements the 74181 ALU, the first commercial ALU, as I did previously in C.

Due to the nature of logic gates, they are most easily represented as small structs with pointers to other gates, potentially holding more gates.

This is very easily respresentable in C, but not in (safe) rust, and having the project be entirely in unsafe rust defeats its purpose, in my opinion.

Therefore, all gates need to be wrapped as such: Rc\<RefCell\<Gate\>\>, which induces a lot of extra checks during runtime, larger binaries and more code to write.

The project is about 20% slower than the C version though, as it needs about 400ms to run 1000 times, compared to 340ms for the C version for the same task (on a ryzen 5 5600x).

The results remain quite impressive, as I have much less Rust experience, and this particular task (inter-referencing structs) is famously not suited for safe rust.
