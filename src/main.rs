use std::env;
use std::process;
use std::cell::RefCell;
use std::rc::Rc;
use mini_computer_rs::Gate;
use mini_computer_rs::GateType;

fn main() {
    let argv: Vec<String> = env::args().collect();

    if argv.len() != 6 || argv[1].len() != 4 || argv[2].len() != 4 || argv[3].len() != 4 || argv[4].len() != 1 || argv[5].len() != 1 {
        eprintln!("Usage: ./alu <4 bits for S inputs> <4 bits for A inputs> <4 bits for B inputs> <1 bit for M> <1 bit for C>");
        process::exit(1);
    }

    let s_args = &argv[1].as_bytes();
    let mut in_s: [Rc<RefCell<Gate>>; 4] = [
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(s_args[0] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(s_args[1] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(s_args[2] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(s_args[3] != b'0'))))
    ];

    let a_args = &argv[2].as_bytes();
    let mut in_a: [Rc<RefCell<Gate>>; 4] = [
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(a_args[0] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(a_args[1] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(a_args[2] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(a_args[3] != b'0'))))
    ];

    let b_args = &argv[3].as_bytes();
    let mut in_b: [Rc<RefCell<Gate>>; 4] = [
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(b_args[0] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(b_args[1] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(b_args[2] != b'0')))),
        Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some(b_args[3] != b'0'))))
    ];

    let mut in_m = Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some((&argv[4].as_bytes())[0] != b'0'))));
    let mut in_c = Rc::new(RefCell::new(Gate::new(GateType::Out, vec!(), Some((&argv[5].as_bytes())[0] != b'0'))));

    let mut not_0: [Rc<RefCell<Gate>>; 5] = [
        Rc::new(RefCell::new(Gate::new(GateType::Not, vec!(Rc::clone(&mut in_m)), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Not, vec!(Rc::clone(&mut in_b[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Not, vec!(Rc::clone(&mut in_b[1])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Not, vec!(Rc::clone(&mut in_b[2])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Not, vec!(Rc::clone(&mut in_b[3])), None)))
    ];

    let mut and_1: [Rc<RefCell<Gate>>; 16] = [
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_a[0]), Rc::clone(&mut in_s[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_s[1]), Rc::clone(&mut not_0[1])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[1]), Rc::clone(&mut in_s[2]), Rc::clone(&mut in_a[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut in_a[0]), Rc::clone(&mut in_s[3]), Rc::clone(&mut in_b[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_b[1]), Rc::clone(&mut in_s[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_s[1]), Rc::clone(&mut not_0[2])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[2]), Rc::clone(&mut in_s[2]), Rc::clone(&mut in_a[1])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut in_a[1]), Rc::clone(&mut in_s[3]), Rc::clone(&mut in_b[1])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_b[2]), Rc::clone(&mut in_s[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_s[1]), Rc::clone(&mut not_0[3])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[3]), Rc::clone(&mut in_s[2]), Rc::clone(&mut in_a[2])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut in_a[2]), Rc::clone(&mut in_s[3]), Rc::clone(&mut in_b[2])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_b[3]), Rc::clone(&mut in_s[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut in_s[1]), Rc::clone(&mut not_0[4])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[4]), Rc::clone(&mut in_s[2]), Rc::clone(&mut in_a[3])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut in_a[3]), Rc::clone(&mut in_s[3]), Rc::clone(&mut in_b[3])), None)))
    ];


    let mut nor_2: [Rc<RefCell<Gate>>; 8] = [
        Rc::new(RefCell::new(Gate::new(GateType::TripleNor, vec!(Rc::clone(&mut in_a[0]), Rc::clone(&mut and_1[0]), Rc::clone(&mut and_1[1])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Nor, vec!(Rc::clone(&mut and_1[2]), Rc::clone(&mut and_1[3])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleNor, vec!(Rc::clone(&mut in_a[1]), Rc::clone(&mut and_1[4]), Rc::clone(&mut and_1[5])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Nor, vec!(Rc::clone(&mut and_1[6]), Rc::clone(&mut and_1[7])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleNor, vec!(Rc::clone(&mut in_a[2]), Rc::clone(&mut and_1[8]), Rc::clone(&mut and_1[9])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Nor, vec!(Rc::clone(&mut and_1[10]), Rc::clone(&mut and_1[11])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleNor, vec!(Rc::clone(&mut in_a[3]), Rc::clone(&mut and_1[12]), Rc::clone(&mut and_1[13])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Nor, vec!(Rc::clone(&mut and_1[14]), Rc::clone(&mut and_1[15])), None)))
    ];

    let mut row_3: [Rc<RefCell<Gate>>; 19] = [
        Rc::new(RefCell::new(Gate::new(GateType::Nand, vec!(Rc::clone(&mut in_c), Rc::clone(&mut not_0[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut nor_2[0]), Rc::clone(&mut nor_2[1])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut nor_2[0])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut nor_2[1]), Rc::clone(&mut in_c)), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut nor_2[2]), Rc::clone(&mut nor_2[3])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut nor_2[2])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut nor_2[0]), Rc::clone(&mut nor_2[3])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuadAnd, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut in_c), Rc::clone(&mut nor_2[1]), Rc::clone(&mut nor_2[3])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut nor_2[4]), Rc::clone(&mut nor_2[5])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut nor_2[4])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut nor_2[2]), Rc::clone(&mut nor_2[5])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuadAnd, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut nor_2[0]), Rc::clone(&mut nor_2[3]), Rc::clone(&mut nor_2[5])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuintAnd, vec!(Rc::clone(&mut not_0[0]), Rc::clone(&mut in_c), Rc::clone(&mut nor_2[1]), Rc::clone(&mut nor_2[3]), Rc::clone(&mut nor_2[5])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut nor_2[6]), Rc::clone(&mut nor_2[7])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuadAnd, vec!(Rc::clone(&mut nor_2[1]), Rc::clone(&mut nor_2[3]), Rc::clone(&mut nor_2[5]), Rc::clone(&mut nor_2[7])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuintAnd, vec!(Rc::clone(&mut in_c), Rc::clone(&mut nor_2[1]), Rc::clone(&mut nor_2[3]), Rc::clone(&mut nor_2[5]), Rc::clone(&mut nor_2[7])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuadAnd, vec!(Rc::clone(&mut nor_2[0]), Rc::clone(&mut nor_2[2]), Rc::clone(&mut nor_2[3]), Rc::clone(&mut nor_2[7])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleAnd, vec!(Rc::clone(&mut not_0[2]), Rc::clone(&mut nor_2[5]), Rc::clone(&mut nor_2[7])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::And, vec!(Rc::clone(&mut nor_2[4]), Rc::clone(&mut nor_2[7])), None)))
    ];

    let mut nor_4: [Rc<RefCell<Gate>>; 4] = [
        Rc::new(RefCell::new(Gate::new(GateType::Nor, vec!(Rc::clone(&mut row_3[2]), Rc::clone(&mut row_3[3])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::TripleNor, vec!(Rc::clone(&mut row_3[5]), Rc::clone(&mut row_3[6]), Rc::clone(&mut row_3[7])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuadNor, vec!(Rc::clone(&mut row_3[9]), Rc::clone(&mut row_3[10]), Rc::clone(&mut row_3[11]), Rc::clone(&mut row_3[12])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::QuadNor, vec!(Rc::clone(&mut row_3[16]), Rc::clone(&mut row_3[17]), Rc::clone(&mut row_3[18]), Rc::clone(&mut nor_2[6])), None)))
    ];

    let mut row_5: [Rc<RefCell<Gate>>; 5] = [
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut row_3[0]), Rc::clone(&mut row_3[1])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut nor_4[0]), Rc::clone(&mut row_3[4])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut nor_4[1]), Rc::clone(&mut row_3[8])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Xor, vec!(Rc::clone(&mut nor_4[2]), Rc::clone(&mut row_3[13])), None))),
        Rc::new(RefCell::new(Gate::new(GateType::Nand, vec!(Rc::clone(&mut row_3[14]), Rc::clone(&mut nor_4[3])), None)))
    ];

    let and_6 = Rc::new(RefCell::new(Gate::new(GateType::QuadAnd, vec!(Rc::clone(&mut row_5[0]), Rc::clone(&mut row_5[1]), Rc::clone(&mut row_5[2]), Rc::clone(&mut row_5[3])), None)));


    let outputs_f = [
        row_5[0].borrow_mut().compute_output().unwrap(),
        row_5[1].borrow_mut().compute_output().unwrap(),
        row_5[2].borrow_mut().compute_output().unwrap(),
        row_5[3].borrow_mut().compute_output().unwrap()
    ];

    let output_p = row_3[14].borrow_mut().compute_output().unwrap();
    let output_c = row_5[4].borrow_mut().compute_output().unwrap();
    let output_g = nor_4[3].borrow_mut().compute_output().unwrap();
    let output_a_b = and_6.borrow_mut().compute_output().unwrap();

    println!("Output F = {}, {}, {}, {}", outputs_f[0], outputs_f[1], outputs_f[2], outputs_f[3]);
    println!("Output P, C, G, 'A = B' = {}, {}, {}, {}", output_p, output_c, output_g, output_a_b);
}
