use std::cell::RefCell;
use std::rc::Rc;

pub enum GateType {
    And,
    TripleAnd,
    QuadAnd,
    QuintAnd,
    Not,
    Xor,
    Nand,
    TripleNand,
    QuadNand,
    QuintNand,
    Nor,
    TripleNor,
    QuadNor,
    Xnor,
    Out,
    Or,
}

pub struct Gate<'a> {
    gates: Vec<Rc<RefCell<Gate<'a>>>>,
    g_type: GateType,
    output: Option<bool>,
}

impl Gate<'_> {
    pub fn new<'a>(g_type: GateType, gates: Vec<Rc<RefCell<Gate<'a>>>>, output: Option<bool>) -> Gate<'a> {
        match g_type {
            GateType::Out => {
                if gates.len() != 0 {
                    panic!("Out gate doesn't get inputs");
                }
                if let None = output {
                    panic!("Out gate needs output to be initialised!");
                }
            },
            GateType::Not => {
                if gates.len() != 1 {
                    panic!("This gate needs one input gates");
                }
                if let Some(_) = output {
                    panic!("Only compute_output can set the output for this gate, set it to None!");
                }
            },
            GateType::And | GateType::Xor | GateType::Nand | GateType::Nor | GateType::Xnor | GateType::Or => {
                if gates.len() != 2 {
                    panic!("This gate needs two input gates");
                }
                if let Some(_) = output {
                    panic!("Only compute_output can set the output for this gate, set it to None!");
                }
            },
            GateType::TripleAnd | GateType::TripleNand | GateType::TripleNor => {
                if gates.len() != 3 {
                    panic!("This gate needs three input gates");
                }
                if let Some(_) = output {
                    panic!("Only compute_output can set the output for this gate, set it to None!");
                }
            },
            GateType::QuadAnd | GateType::QuadNand | GateType::QuadNor => {
                if gates.len() != 4 {
                    panic!("This gate needs four input gates");
                }
                if let Some(_) = output {
                    panic!("Only compute_output can set the output for this gate, set it to None!");
                }
            },
            GateType::QuintAnd | GateType::QuintNand => {
                if gates.len() != 5 {
                    panic!("This gate needs five input gates");
                }
                if let Some(_) = output {
                    panic!("Only compute_output can set the output for this gate, set it to None!");
                }
            }
        }
        Gate {
            g_type,
            gates,
            output
        }
    }

    pub fn compute_output(&mut self) -> Option<bool> {
        match self.g_type {
            GateType::And => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                self.output = Some(out_0 & out_1);
            },
            GateType::TripleAnd => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                self.output = Some(out_0 & out_1 & out_2);
            },
            GateType::QuadAnd => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                let out_3 = self.gates[3].borrow_mut().compute_output().unwrap();
                self.output = Some(out_0 & out_1 & out_2 & out_3);
            },
            GateType::QuintAnd => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                let out_3 = self.gates[3].borrow_mut().compute_output().unwrap();
                let out_4 = self.gates[4].borrow_mut().compute_output().unwrap();
                self.output = Some(out_0 & out_1 & out_2 & out_3 & out_4);
            },
            GateType::Or => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                self.output = Some(out_0 | out_1);
            },
            GateType::Xor => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                self.output = Some(out_0 ^ out_1);
            },
            GateType::Not => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                self.output = Some(!out_0);
            },
            GateType::Nand => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 & out_1));
            },
            GateType::TripleNand => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 & out_1 & out_2));
            },
            GateType::QuadNand => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                let out_3 = self.gates[3].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 & out_1 & out_2 & out_3));
            },
            GateType::QuintNand => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                let out_3 = self.gates[3].borrow_mut().compute_output().unwrap();
                let out_4 = self.gates[4].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 & out_1 & out_2 & out_3 & out_4));
            },
            GateType::Nor => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 | out_1));
            },
            GateType::Xnor => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 ^ out_1));
            },
            GateType::TripleNor => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 | out_1 | out_2));
            },
            GateType::QuadNor => {
                let out_0 = self.gates[0].borrow_mut().compute_output().unwrap();
                let out_1 = self.gates[1].borrow_mut().compute_output().unwrap();
                let out_2 = self.gates[2].borrow_mut().compute_output().unwrap();
                let out_3 = self.gates[3].borrow_mut().compute_output().unwrap();
                self.output = Some(!(out_0 | out_1 | out_2 | out_3));
            },
            GateType::Out => {},
        }
        self.output
    }
}
